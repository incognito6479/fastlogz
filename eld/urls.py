from django.urls import path, include
from .view import views, eld, vehicles, drivers


drivers_url = [
    path('', drivers.ShowDriversView.as_view()),
    path('<int:pk>/', drivers.ShowDriversView.as_view()),
]

eld_url = [
    path('', eld.Eld.as_view()),
    path('<int:pk>/', eld.Eld.as_view()),
]

vehicles_url = [
    path('', vehicles.Vehicles.as_view()),
    path('<int:pk>/', vehicles.Vehicles.as_view()),
]

register_logout_url = [
    path('register/', views.RegisterView.as_view()),
    path('logout/', views.LogoutView.as_view()),
    path('validate_activation_code/', views.CheckActivationCodeView.as_view()),
]

urlpatterns = [
    path('eld/', include(eld_url)),
    path('drivers/', include(drivers_url)),
    path('vehicles/', include(vehicles_url)),
    path('', include(register_logout_url))
]
