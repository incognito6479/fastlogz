from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from rest_framework.response import Response
from rest_framework.views import APIView
from .. import models, serializers
import json


class ShowDriversView(APIView):
    def get(self, request):
        drivers = models.DRIVERS.objects.all()
        drivers_status_false = models.DRIVERS.objects.filter(status=True)
        reserved_vehicle_id = []
        for i in drivers_status_false:
            reserved_vehicle_id.append(i.vehicle_id_id)
        total_vehicle_id = []
        for i in models.Vehicle.objects.all():
            total_vehicle_id.append(i.vehicle_id)
        available_vehicle_id = []
        for i in total_vehicle_id:
            if i not in reserved_vehicle_id:
                available_vehicle_id.append(int(i))
        if not drivers:
            context = {
                'data': {
                    'status': 'empty',
                    'available_vehicle_id': json.dumps(available_vehicle_id)
                }
            }
            return Response(context)
        serializer = serializers.DriverSerializer(drivers, many=True)
        context = {
            'data': {
                'drivers': serializer.data,
                'available_vehicle_id': json.dumps(available_vehicle_id)
            }
        }
        return Response(context)

    def post(self, request):
        check_vehicle_id = models.DRIVERS.objects.filter(vehicle_id=request.data['vehicle_id'], status=True)
        check_email = models.DRIVERS.objects.filter(email=request.data['email'], status=True)
        if not check_email:
            if not check_vehicle_id:
                serializer = serializers.DriverSerializer(data=request.data)
                if serializer.is_valid():
                    password_hash = make_password(request.data['password'])
                    user = get_user_model()
                    driver = user(email=str(request.data['email']), password=password_hash)
                    driver.is_staff = False
                    driver.is_superuser = False
                    driver.save()
                    serializer.save()
                    return Response({'data': {'status': 'created'}})
                return Response({'data': {'status': 'error', 'errors': serializer.errors}})
            return Response({'data': {'status': 'error', 'cause': 'this vehicle_id is already used'}})
        return Response({'data': {'status': 'error', 'cause': 'email already exists'}})

    def put(self, request, pk):
        try:
            driver = models.DRIVERS.objects.get(id=pk)
            try:
                user = get_user_model()
                obj = user.objects.get(email=driver.email)
                serializer = serializers.DriverSerializer(instance=driver, data=request.data)
                if serializer.is_valid():
                    obj.email = request.data['email']
                    obj.password = make_password(request.data['password'])
                    obj.save()
                    serializer.save()
                    return Response({'data': {'status': 'edited'}})
                return Response({'data': {'status': 'error', 'errors': serializer.errors}})
            except:
                return Response({'data': {'status': 'error', 'cause': 'no such email'}})
        except:
            return Response({'data': {'status': 'error', 'cause': 'no such driver found'}})

    def delete(self, request, pk):
        try:
            driver = models.DRIVERS.objects.get(id=pk)
            user = get_user_model()
            delete_driver_from_custom_user = user.objects.get(email=driver.email)
            delete_driver_from_custom_user.delete()
            driver.status = False
            driver.save()
            return Response({'data': {'status': 'driver deactivated'}})
        except:
            return Response({'data': {'status': 'error', 'cause': 'driver already deactivated'}})