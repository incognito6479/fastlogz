from rest_framework.response import Response
from rest_framework.views import APIView
from .. import models, serializers
import json


class Vehicles(APIView):
    def get(self, request):
        vehicles = models.Vehicle.objects.all().order_by('-vehicle_id')
        eld = models.ELD.objects.all()
        reserved_eld = []
        for i in models.Vehicle.objects.all():
            reserved_eld.append(i.eld_id_id)
        total_eld = []
        for i in eld:
            total_eld.append(i.id)
        available_eld_id_list = []
        for i in total_eld:
            if i not in reserved_eld:
                available_eld_id_list.append(i)
        if not vehicles:
            return Response({'data': {
                'status': 'empty',
                'available_eld_id': json.dumps(available_eld_id_list)
            }})
        serializer = serializers.VehicleSerializer(vehicles, many=True)
        context = {
            'data': {
                'vehicles': serializer.data,
                'available_eld_id': json.dumps(available_eld_id_list)
            }
        }
        return Response(context)

    def post(self, request):
        check_license_plate_num = models.Vehicle.objects.filter(license_plate_num=int(request.data['license_plate_num']))
        check_eld_id = models.Vehicle.objects.filter(eld_id=int(request.data['eld_id']))
        check_vehicle_id = models.Vehicle.objects.filter(vehicle_id=str(request.data['vehicle_id']))
        if not check_vehicle_id:
            if not check_eld_id:
                if not check_license_plate_num:
                    serializer = serializers.VehicleSerializer(data=request.data)
                    if serializer.is_valid():
                        serializer.save()
                        return Response({'data': {'status': 'created'}})
                    return Response({'data': {'status': 'error', 'errors': serializer.errors}})
                return Response({'data': {'status': 'error', 'cause': 'license_plate_num already exists'}})
            return Response({'data': {'status': 'error', 'cause': 'this eld_id is used by another vehicle'}})
        return Response({'data': {'status': 'error', 'cause': 'this vehicle_id already exists'}})

    def put(self, request, pk):
        try:
            vehicle = models.Vehicle.objects.get(id=pk)
            serializer = serializers.VehicleSerializer(instance=vehicle, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'data': {'status': 'edited'}})
            return Response({'data': {'status': 'error', 'errors': serializer.errors}})
        except:
            return Response({'data': {'status': 'error', 'cause': 'vehicle not found'}})

    def delete(self, request, pk):
        try:
            vehicle = models.Vehicle.objects.get(vehicle_id=pk)
            vehicle.delete()
            return Response({'data': {'status': 'deleted'}})
        except:
            return Response({'data': {'status': 'no vehicle found or vehicle used by other driver'}})