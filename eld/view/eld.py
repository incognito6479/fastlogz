from rest_framework.response import Response
from rest_framework.views import APIView
from .. import models, serializers


class Eld(APIView):
    # returns list of elds
    def get(self, request):
        eld = models.ELD.objects.all().order_by('-id')
        if not eld:
            return Response({'data': {'status': 'empty'}})
        serializer = serializers.EldSerializer(eld, many=True)
        context = {
            'data': {
                'eld': serializer.data,
            }
        }
        return Response(context)

    # adds eld to database
    def post(self, request):
        check_eld = models.ELD.objects.filter(serial_number=str(request.data['serial_number']))
        if not check_eld:
            serializer = serializers.EldSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'data': {'status': 'created'}})
            return Response({'data': {'status': 'error', 'errors': serializer.errors}})
        return Response({'data': {'status': 'error', 'cause': 'ELD already exists'}})

    # edits eld by given id-pk
    def put(self, request, pk):
        try:
            eld = models.ELD.objects.get(id=pk)
            serializer = serializers.EldSerializer(instance=eld, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'data': {'status': 'edited'}})
            return Response({'data': {'status': 'error', 'errors': serializer.errors}})
        except:
            return Response({'data': {'status': 'error', 'cause': 'eld not found'}})

    # deletes eld by given id-pk
    def delete(self, request, pk):
        eld = models.ELD.objects.filter(id=pk)
        if not eld:
            return Response({'data': {'status': 'empty'}})
        eld.delete()
        return Response({'data': {'status': 'deleted'}})