from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework_simplejwt.tokens import RefreshToken
from .. import random


class RegisterView(APIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []

    def post(self, request):
        user = get_user_model()
        check_email = user.objects.filter(email=request.data['email'])
        if not check_email:
            try:
                code = random.generate()
                send_mail('Activation code', 'Your activation code is: ' + code, 'no-replay@gmail.com',
                          [request.data['email']], fail_silently=False)
                request.session['activation_code'] = code
                return Response({'data': {'status': 'activation code is sent'}})
            except:
                return Response({'data': {'status': 'failed to send activation code'}})
        return Response({'data': {'status': 'error', 'cause': 'email already exists'}})


class LogoutView(APIView):
    permission_classes = [permissions.AllowAny]

    def post(self, request):
        try:
            refresh_token = request.data['refresh_token']
            token_remove = RefreshToken(refresh_token)
            token_remove.blacklist()
            return Response({'data': {'status': 'logged out'}})
        except:
            return Response({'data': {'status': 'error'}})


class CheckActivationCodeView(APIView):
    permission_classes = [permissions.AllowAny]

    def post(self, request):
        if request.data['activation_code'] == request.session['activation_code']:
            user = get_user_model()
            new_user = user(email=request.data['email'], password=make_password(request.data['password']))
            new_user.is_staff = True
            new_user.is_superuser = True
            new_user.is_active = True
            new_user.save()
            return Response({'data': {'status': 'user registered'}})
        return Response({'data': {'status': 'error', 'cause': "activation code didn't match"}})