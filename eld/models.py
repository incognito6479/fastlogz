from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _

# def year_choices():
#     return tuple((r,r) for r in range(2000, datetime.date.today().year))
#
#
# def current_year():
#     return datetime.date.today().year
#
#
# states = (
#      ('AL', 'AL'),
#      ('Alaska', 'AK'),
#      ('Arizona', 'AZ'),
#      ('Arkansas', 'AR'),
#      ('California', 'CA'),
#      ('Colorado', 'CO'),
#      ('Connecticut', 'CT'),
#      ('Delaware', 'DE'),
#      ('District', 'DC'),
#      ('Florida', 'FL'),
#      ('Georgia', 'GA'),
#      ('Hawaii', 'HI'),
#      ('Idaho', 'ID'),
#      ('Illinois', 'IL'),
#      ('Indiana', 'IN'),
#      ('Iowa', 'IA'),
#      ('Kansas', 'KS'),
#      ('Kentucky', 'KY'),
#      ('Louisiana', 'LA'),
#      ('Maine', 'ME'),
#      ('Maryland', 'MD'),
#      ('Massachusetts', 'MA'),
#      ('Michigan', 'MI'),
#      ('Minnesota', 'MN'),
#      ('Mississippi', 'MS'),
#      ('Missouri', 'MO'),
#      ('Montana', 'MT'),
#      ('Nebraska', 'NE'),
#      ('Nevada', 'NV'),
#      ('New', 'NH'),
#      ('New', 'NJ'),
#      ('New', 'NM'),
#      ('New', 'NY'),
#      ('North', 'NC'),
#      ('North', 'ND'),
#      ('Ohio', 'OH'),
#      ('Oklahoma', 'OK'),
#      ('Oregon', 'OR'),
#      ('Pennsylvania', 'PA'),
#      ('Rhode', 'RI'),
#      ('South', 'SC'),
#      ('South', 'SD'),
#      ('Tennessee', 'TN'),
#      ('Texas', 'TX'),
#      ('Utah', 'UT'),
#      ('Vermont', 'VT'),
#      ('Virginia', 'VA'),
#      ('Washington', 'WA'),
#      ('West', 'WV'),
#      ('Wisconsin', 'WI'),
#      ('Wyoming', 'WY'),
# )
#
# fuels = (
#     (1, 'gasoline'),
#     (2, 'diesel'),
#     (3, 'off-road diesel'),
#     (4, 'propane',),
#     (5, 'compressed natural gas'),
#     (6, 'ethanol'),
#     (7, 'kerosine'),
#     (8, 'other'),
# )

# Create superuser with email instead of username


class CustomUserManager(BaseUserManager):
    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """
    def create_user(self, email, password, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError(_('The Email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, password, **extra_fields)


class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email


class ELD(models.Model):
    serial_number = models.CharField(max_length=30)
    notes_eld = models.CharField(blank=True, null=True, default='', max_length=200)

    def __str__(self):
        return self.serial_number


class Vehicle(models.Model):
    id = models.IntegerField(primary_key=True)
    vehicle_id = models.CharField(unique=True, max_length=200)
    make = models.CharField(max_length=20)
    model = models.CharField(max_length=20)
    year = models.IntegerField()
    license_plate_num = models.IntegerField(unique=True)
    license_plate_issue_state = models.CharField(max_length=100)
    vin = models.CharField(blank=True, max_length=30, null=True)
    eld_id = models.OneToOneField(ELD, unique=True, on_delete=models.DO_NOTHING)
    notes_vehicle = models.CharField(blank=True, null=True, default='', max_length=200)

    def __str__(self):
        return self.model


class DRIVERS(models.Model):
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=250)
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length=30)
    driver_license_number = models.CharField(max_length=250)
    dr_li_issue_state = models.CharField(max_length=100)
    co_driver = models.CharField(blank=True, null=True, max_length=100)
    home_terminal_address = models.CharField(max_length=200, null=True, blank=True)
    home_terminal_time_zone = models.CharField(max_length=200)
    vehicle_id = models.ForeignKey(Vehicle, on_delete=models.DO_NOTHING)
    status = models.BooleanField(default=True)
    trail_number = models.IntegerField()
    enable_dr_eld = models.BooleanField(default=False)
    enable_dr_e_log = models.BooleanField(default=False)
    allow_yard = models.BooleanField(default=False)
    allow_personal_c = models.BooleanField(default=False)
    notes_driver = models.CharField(blank=True, null=True, default='', max_length=200)

    def __str__(self):
        return self.name


class Company(models.Model):
    organization_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    region = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    postal_code = models.CharField(max_length=200)
    home_terminal_time_zone = models.CharField(max_length=200)
    MC = models.CharField(max_length=300)
    contact_person = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)
    email = models.EmailField(unique=True)
