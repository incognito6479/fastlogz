from rest_framework import serializers
from . import models


class EldSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ELD
        fields = '__all__'


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Vehicle
        fields = '__all__'


class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DRIVERS
        fields = '__all__'