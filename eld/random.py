import random


def generate():
    code = []
    code_string = ''
    for i in range(1, 7):
        n = random.randint(1, 9)
        code.append(str(n))
    for j in code:
        code_string += j
    return code_string
